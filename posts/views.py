from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView

try:
    from posts.models import Post
except Exception:
    Post = None


# Create your views here.


class PostListView(ListView):
    model = Post
    context_object_name = "posts"
    template_name = "posts/list.html"


class PostDetailView(DetailView):
    model = Post
    context_object_name = "post"
    template_name = "posts/detail.html"


class PostCreateView(CreateView):
    model = Post
    fields = ["title", "content", "created_at"]
    template_name = "posts/create.html"
    success_url = "/"


class PostUpdateView(UpdateView):
    model = Post
    fields = ["title", "content", "created_at"]
    template_name = "posts/update.html"
    success_url = "/"
